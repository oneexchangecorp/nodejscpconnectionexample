const WebSocket = require('ws');
var cookie = require('cookie');
var https = require('https')

const domain = "REPLACE WITH CLIENT PORTAL API DOMAIN";
const username = "REPLACE WITH USERNAME";
const password = "REPLACE WITH PASSWORD";


var AuthToken = "";

authAndConnect();

function authAndConnect() {
    // form data
    postData = JSON.stringify({
        username: username,
        password: password
    })

    // request option
    options = {
        host: domain,
        port: 443,
        method: 'POST',
        path: '/api/login',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': postData.length
        }
    };

    // request object
    var req = https.request(options, function (res) {
        responseString = '';
        res.on('data', function (chunk) {
            responseString += chunk;
        });
        res.on('end', function () {

            try {
                var obj = JSON.parse(responseString);

                if (obj) {
                    if (obj.error) {
                        console.log(obj.error);
                    }
                    else if (obj.token) {
                        console.log(obj.token);

                        AuthToken = obj.token;
                        setupWebsocket()

                        getConfirmsByDateRange()

                        getSettlesByDate()

                        getIndicesByDate()
                    }

                }
            }
            catch (e) {
                console.log(e);
                reconnect();
            }

        });
        res.on('error', function (err) {
            console.log(err);
            reconnect();
        })
    });

    // req error
    req.on('error', function (err) {
        console.log(err);
        reconnect();
    });

    //send request witht the postData form
    req.write(postData);
    req.end();

}

function reconnect() {
    let reloadTimeout = Math.floor(Math.random() * 5000) + 3000
    setTimeout(authAndConnect, reloadTimeout);
}

function setupWebsocket() {
    const ws = new WebSocket('wss://' + domain,
        [],
        {
            'headers': {
                'Cookie': [cookie.serialize('token', AuthToken)
                ]
            }
        });

    ws.on('open', function open() {
        console.log('open');
    });

    ws.on('message', function incoming(data) {
        console.log(data);
    });

    ws.on('close', function close() {
        console.log('disconnected');

        reconnect();
        return;
    });
}


function getConfirmsByDateRange()
{
    console.log('***** getConfirmsByDateRange *****');
    // request option
    options = {
        hostname: domain,
        port: 443,
        path: '/api/v1/confirms?from_date=02/07/2020&to_date=02/07/2020',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+AuthToken
        }
      }
     
      const req = https.request(options, (res) => {
        console.log(`statusCode: ${res.statusCode}`)

        responseString = '';

        res.on('data', (d) => {
            responseString += d;
        })

        res.on('end', function () {
            console.error(responseString)
            console.log('***** END getConfirmsByDateRange *****');
        });
      })
      
      req.on('error', (error) => {
        console.error(error)
      })

     
      
      req.end()

     
}

function getSettlesByDate()
{
    console.log('***** getSettlesByDate *****');
    // request option
    options = {
        hostname: domain,
        port: 443,
        path: '/api/v1/settles?settle_date=06/17/2020',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+AuthToken
        }
      }
      
      const req = https.request(options, (res) => {
        console.log(`statusCode: ${res.statusCode}`)
      
        responseString = '';

        res.on('data', (d) => {
            responseString += d;
        })

        res.on('end', function () {
            console.error(responseString)
            console.log('***** END getSettlesByDate *****');
        });
      })
      
      req.on('error', (error) => {
        console.error(error)
      })
      
      req.end()
}

function getIndicesByDate()
{
    console.log('***** getIndicesByDate *****');
    // request option
    options = {
        hostname: domain,
        port: 443,
        path: '/api/v1/cci_index?date=05/06/2020',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+AuthToken
        }
      }
      
      const req = https.request(options, (res) => {
        console.log(`statusCode: ${res.statusCode}`)
      
        responseString = '';

        res.on('data', (d) => {
            responseString += d;
        })

        res.on('end', function () {
            console.error(responseString)
            console.log('***** END getIndicesByDate *****');
        });
      })
      
      req.on('error', (error) => {
        console.error(error)
      })
      
      req.end()
}



