# NodeJS Client Portal Connection Example

A Node.js implementation of how to connect to the One Exchange Client Portal 

## Installation

1. Install Node.js v8.9.4
2. Clone repository
3. replace domain, username and password with values provided by One Exchange in the index.js file.
4. Change to nodejscpconnectionexample directory in command line environment
5. type `run npm install` into install the required libraries.


## Usage

type `node .\index.js` to run the application.